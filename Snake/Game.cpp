#include "Game.h"

Game::Game() 
	: window(std::make_unique<GameWindow>(sf::Vector2u(800, 640), "Snake" )),
	  world(std::make_unique<World>(window->getWindowSize())),
	  snake(std::make_unique<Snake>(world->getBlockSize()))
{
	//It has to be here cause init has to be after clock restart
	clock.restart();
	elapsed = sf::seconds(0.0f);
	logTimeAcc = sf::seconds(0.0f);
}

void Game::handleInput() const
{
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up)
		&& snake->getPhysDir() != sf::Vector2i(0,1))
	{
		snake->setDirection(0, -1);
	}
	else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down)
			&& snake->getPhysDir() != sf::Vector2i(0,-1))
	{
		snake->setDirection(0, 1);
	}
	else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left)
			&& snake->getPhysDir() != sf::Vector2i(1,0))
	{
		snake->setDirection(-1, 0);
	}
	else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right)
			&& snake->getPhysDir() != sf::Vector2i(-1,0))
	{
		snake->setDirection(1, 0);
	}

}

void Game::update()
{
	window->update();
	if (logTimeAcc.asSeconds() >= logicTime.asSeconds())
	{
		snake->update();
		world->update(*snake);
		logTimeAcc -= logicTime;
		if(snake->hasLost())
		{
			snake.reset();
		}
	}
}

void Game::render() const
{
	window->clear();
	world->render(*window);
	snake->render(*window);
	window->endDraw();
}

void Game::restartClock()
{
	elapsed = clock.restart();
	logTimeAcc += elapsed;
}

bool Game::isRunning() const
{
	return window->isClosed();
}

sf::Time Game::getElapsed() const
{
	return elapsed;
}