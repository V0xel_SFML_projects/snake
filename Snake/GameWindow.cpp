#include "GameWindow.h"
#include <SFML/Window/Event.hpp>

GameWindow::GameWindow(const sf::Vector2u& winSize, 
					const std::string&& winTitle)
			: windowSize(winSize), windowTitle(winTitle)
{
	window.setFramerateLimit(60);
	create();
}

GameWindow::~GameWindow()
{
	window.close();
}

bool GameWindow::isClosed() const
{
	return closed;
}

bool GameWindow::isFullscreen() const
{
	return winStyle;
}

sf::Vector2u GameWindow::getWindowSize() const
{
	return windowSize;
}

void GameWindow::create()
{
	const auto style = (winStyle ? sf::Style::Fullscreen : sf::Style::Default );
	window.create({windowSize.x, windowSize.y, 32}, windowTitle, style);
}

void GameWindow::update() //event processing
{
	sf::Event event;
	while (window.pollEvent(event))
	{
		if (event.type == sf::Event::Closed)
		{
			closed=true;
		}
		else if(event.type == sf::Event::KeyPressed && 
				event.key.code == sf::Keyboard::F5)
		{
			toggleFullscreen();
		}
	}
}

void GameWindow::draw(sf::Drawable& drawable)
{
	window.draw(drawable);
}

void GameWindow::clear() 
{
	window.clear(sf::Color::Green);
}

void GameWindow::endDraw() //put what has been drawn to canvas
{
	window.display();
}

void GameWindow::toggleFullscreen()
{
	winStyle = !winStyle;
	window.close();
	create();
}

