#include <Windows.h>
#include <memory>
#include  "Game.h"
#include <SFML/System.hpp>

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int windowStyle)
{
	const sf::Time frameTime(sf::seconds(0.0016f));
	auto game = std::make_unique<Game>();

	while (!game->isRunning())
	{
		game->handleInput();
		game->update();
		game->render();
		game->restartClock(); //to know how much time has the loop iteration taken 
		if (game->getElapsed().asSeconds() < frameTime.asSeconds())
		{
			sf::sleep(sf::seconds(frameTime.asSeconds() - game->getElapsed().asSeconds())); //sleep the difference
		} //the sleep time wont interfere with clock, cause it wont be added as clock is measuring process time, not an absolute time
	}
	return 0;
}