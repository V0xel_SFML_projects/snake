#pragma once
#include <SFML/System/Vector2.hpp>
#include <SFML/Graphics/RectangleShape.hpp>
#include "GameWindow.h"
#include <vector>

struct SnakeSegment
{
	SnakeSegment(const unsigned int& x, const unsigned int& y) : position(x,y){}
	sf::Vector2u position;
};

using snakeVec = std::vector<SnakeSegment>;

class Snake
{
public:
	explicit Snake(const int& blockSize);
	~Snake();

	int getSpeed() const;
	int getLives() const;
	sf::Vector2i getPhysDir() const;
	sf::Vector2u getHeadPos() const;
	void setDirection(const int& x, const int& y);
	bool hasLost() const;

	void lose();
	void toggleLose();
	void extend();
	void reset();
	void move();
	void update();
	void cut(const int& segments);
	void render(GameWindow& window);

private:
	unsigned int speed	{30};
	unsigned int lives	{2};
	unsigned int size	{0};
	bool lost			{false};

	sf::Vector2i curDir {0,0};	  //2D unit vector
	sf::RectangleShape bodyBlock; //shape in rendering
	snakeVec body;

	void selfCollision();
};