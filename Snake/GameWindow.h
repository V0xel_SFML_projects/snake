#pragma once
#include<string>
#include <SFML/System/Vector2.hpp>
#include <SFML/Graphics/Drawable.hpp>
#include <SFML/Graphics/RenderWindow.hpp>

/*TODO: Change class in a way that only public functions are create and draw,
 * and draw should take a ptr to drawable array/vect/whatever and draw everything in that
 */

class GameWindow
{
public:
	GameWindow(const sf::Vector2u&, const std::string&&);
	GameWindow(const GameWindow&) = delete;
	GameWindow(const GameWindow&&) = delete;
	GameWindow& operator=(const GameWindow&) = delete;
	GameWindow& operator=(const GameWindow&&) = delete;
	~GameWindow();

	void clear();
	void endDraw();
	void update();
	void toggleFullscreen();
	void draw(sf::Drawable& drawable);

	bool isClosed() const;
	bool isFullscreen() const;
	sf::Vector2u getWindowSize() const;

private:
	sf::RenderWindow window;
	sf::Vector2u windowSize		{640, 480};
	std::string windowTitle		{"Snake Game"};

	void create();

	bool closed		{false};
	bool winStyle	{false};
};
