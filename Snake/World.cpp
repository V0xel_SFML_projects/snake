﻿#include "World.h"

World::World(const sf::Vector2u& windSize)
	: maxPosU(windSize.x/blockSize, windSize.y/blockSize)
{
	border.setFillColor		(sf::Color::Black);
	border.setOutlineColor	(sf::Color::Blue);
	border.setOrigin		(0.0f, 0.0f);
	border.setPosition		(0, 0);
	border.setSize			({float(windSize.x), float(windSize.y)});
	appleShape.setRadius	(boardAbsT/2);
	appleShape.setFillColor	(sf::Color::Red);
	spawnApple();
}

void World::spawnApple()
{
	applePos = {unsigned(rng.randomNum(1, maxPosU.x - 2)), unsigned(rng.randomNum(1, maxPosU.y - 2))};
	appleShape.setPosition(applePos.x*blockSize, applePos.y*blockSize);
}

void World::update(Snake& snake)
{
	if (snake.getHeadPos() == applePos)
	{
		snake.extend();
		score += 10;
		spawnApple();
	}
	//TODO: Later changed to allow collision with boxes
	const unsigned int hX = snake.getHeadPos().x;
	const unsigned int hY = snake.getHeadPos().y;

	if (hX <= 0 || hY <= 0 || 
		hX >= maxPosU.x - 1 || hY >= maxPosU.y - 1)
	{
		snake.lose();
	}
}

void World::render(GameWindow& window)
{
	
	border.setOutlineThickness(boardThickness);
	window.draw(border);
	window.draw(appleShape);
}

unsigned World::getScore() const
{
	return score;
}

unsigned World::getBlockSize() const
{
	return blockSize;
}

