#pragma once
#include <memory>
#include "GameWindow.h"
#include "World.h"
#include "Snake.h"

class Game
{
public:
	Game();
	~Game() = default;
	void handleInput() const;
	void update();
	void render() const;
	void restartClock();

	bool isRunning() const;
	sf::Time getElapsed() const;

private:
	std::unique_ptr<GameWindow> window;
	std::unique_ptr<World>world;
	std::unique_ptr<Snake>snake;
	sf::Clock clock;
	sf::Time elapsed;
	sf::Time logTimeAcc;
	sf::Time logicTime{sf::seconds(0.055f)};
};

