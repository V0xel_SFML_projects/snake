﻿#include "RNG.h"

int RNG::randomNum(const int& begin, const int& end)
{
	const std::uniform_int_distribution<int>distribution(begin, end);
		return distribution(rng);
}

float RNG::randomNum(const float& begin, const float& end)
{
	const std::uniform_real_distribution<float>distribution(begin, end);
		return distribution(rng);
}
