#include "Snake.h"

Snake::Snake(const int& blockSize)
	: size(blockSize)
{
	bodyBlock.setSize({float(size - 1), float(size - 1)}); //-1 to show separation of segments
	reset();
}

Snake::~Snake() = default;

int Snake::getSpeed() const
{
	return speed;
}

int Snake::getLives() const
{
	return lives;
}

bool Snake::hasLost() const
{
	return lost;
}

sf::Vector2i Snake::getPhysDir() const
{
	if (body.size() <= 1)
	{
		return {0, 0};
	}
	//Same logic as in extend func
	const SnakeSegment& head = body[0];
	const SnakeSegment& neck = body[1];
	if (head.position.x == neck.position.x)
	{
		return head.position.y > neck.position.y
			? sf::Vector2i(0, 1) : sf::Vector2i(0, -1);
	}
	if (head.position.y == neck.position.y)
	{
		return head.position.x > neck.position.x
			? sf::Vector2i(1,0) : sf::Vector2i(-1,0);
	}
	return {0,0};
}

sf::Vector2u Snake::getHeadPos() const
{
	return body.front().position;
}

//Resets snake to 3 squares on same Y axis
void Snake::reset()
{
	body.clear();
	body.push_back({24, 20});
	body.push_back({25,20});
	body.push_back({26,20});
	curDir.x = 0;
	curDir.y = 0;
}

void Snake::setDirection(const int& x, const int& y)
{
	curDir.x = x;
	curDir.y = y;
}

void Snake::lose()
{
	lost = true;
}

void Snake::toggleLose()
{
	lost = !lost;
}

void Snake::extend()
{
	if (body.empty()) { return; }
	SnakeSegment& last = body[body.size() - 1];

	if (body.size() > 1)
	{
		SnakeSegment& preLast = body[body.size() - 2];
		const float newX = last.position.x - preLast.position.x;
		const float newY = last.position.y - preLast.position.y;

		if (newX == 0)
		{
			if (newY > 0)
			{
				body.push_back({last.position.x, last.position.y + 1});
			}
			else
			{
				body.push_back({last.position.x, last.position.y - 1});
			}
		}
		else
		{
			if (newX > 0)
			{
				body.push_back({last.position.x + 1, last.position.y});
			}
			else
			{
				body.push_back({last.position.x - 1, last.position.y});
			}
		}
	}
	else
	{
		body.push_back({last.position.x - curDir.x, last.position.y - curDir.y}); //reverse of the current direction, also last is first here
	}
}

void Snake::move()
{
	for (size_t i = body.size() - 1; i > 0; --i) //iterating backwards for inchworm effect
	{
		body[i].position = body[i - 1].position;
	}

	body[0].position.x += curDir.x;
	body[0].position.y += curDir.y;
}

void Snake::update()
{
	if (body.empty() || 
		(curDir.x == 0 && curDir.y == 0)) { return; }
	move();
	selfCollision();
}

void Snake::cut(const int& segments)
{
	for (std::size_t i = 0; i < segments; ++i)
	{
		body.pop_back();
	}
	--lives;
	if (!lives)
	{
		lose();
	}
}

void Snake::render(GameWindow& window)
{
	if (body.empty()) { return; }

	const auto head = body.begin();
	bodyBlock.setFillColor(sf::Color::Yellow);
	bodyBlock.setPosition(head->position.x * size, head->position.y * size);
	window.draw(bodyBlock);

	bodyBlock.setFillColor(sf::Color::Green);
	for (auto itr = body.begin() + 1; itr != body.end(); ++itr)
	{
		bodyBlock.setPosition(itr->position.x * size, itr->position.y * size);
		window.draw(bodyBlock);
	}
}

void Snake::selfCollision()
{
	if (body.size() < 5) { return; }
	SnakeSegment& head = body.front();
	for (auto itr = body.begin() + 1; itr != body.end(); ++itr) //start from second
	{
		if (itr->position == head.position)			//check if pos are the same
		{
			const auto segments = body.end() - itr; //number of seg between end and current itrs
			cut(segments);
			break;
		}
	}
}
