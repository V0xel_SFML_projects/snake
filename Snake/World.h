﻿#pragma once
#include <SFML/Graphics.hpp>
#include "Snake.h"
#include "GameWindow.h"
#include "RNG.h"

class World
{
public:
	 World(const sf::Vector2u&);
	~World() = default;

	unsigned getScore() const;
	unsigned getBlockSize() const;

	void spawnApple();
	void update(Snake& snake);
	void render(GameWindow& window);

private:
	unsigned int score		{0};
	unsigned int blockSize	{16};
	int boardThickness		{-16};	//neg val = inner border
	const int boardAbsT		{abs(boardThickness)};
	RNG rng;						//on stack cause it weights "nothing"
	sf::Vector2u applePos;
	sf::Vector2u maxPosU;
	sf::RectangleShape border;
	sf::CircleShape appleShape;
};
