﻿#pragma once
#include <random>
class RNG
{
public:
	int randomNum(const int&, const int&);
	float randomNum(const float&, const float&);
private:
	std::mt19937 rng{ std::random_device{}() };
};
